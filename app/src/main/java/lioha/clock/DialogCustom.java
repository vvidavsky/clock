package lioha.clock;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TimePicker;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import lioha.clock.DataBase.DBHandler;

/**
 * Created by vladvidavsky on 10/23/16.
 */

class DialogCustom {
    private Context cont;
    private Activity activity;
    public Dialog dialog;
    private DBHandler dbh;
    private ToastCustom tc;
    private UIOperations ui;
    private String password, url, rowId, manualTimeFlag;
    static Boolean isOnSettingsScreen = false;

    DialogCustom(Activity act) {
        cont = act;
        activity = act;
        dialog = new Dialog(act);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dbh = new DBHandler(act, act);
        tc = new ToastCustom(act);
        ui = new UIOperations(act);
    }

    /**
     * Данный Dialog показывается когда пользователь
     * нажимает на кнопку Settings чтобы войти в меню настроек,
     * при нажатии выскакиевает окошко где нужно внести пароль для того чтобы
     * продолжить дальше.
     */
    void showSettingsPasswordDialog() {
        dialog.setContentView(R.layout.screen_password);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        final Button closeDialog = (Button) dialog.findViewById(R.id.close_pwd_dialog);
        Button getIn = (Button) dialog.findViewById(R.id.get_in);
        final EditText passwordField = (EditText) dialog.findViewById(R.id.password_field);


        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ui.setAppFullView(activity);
            }
        });
        getIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                password = dbh.getSettingsItemAtIndex(1);
                String insertedPassword = passwordField.getText().toString();
                if (insertedPassword.length() > 0) {
                    if (password.equals(insertedPassword) || Constants.ADMIN_PWD.equals(insertedPassword)) {
                        dialog.dismiss();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                showSettingsDialog();
                            }
                        }, 100);
                    } else {
                        tc.showCustomToast(9, null);
                    }
                } else {
                    tc.showCustomToast(8, null);
                }
            }
        });
        dialog.show();
    }

    /**
     * Данный метод открывает экран настроек аппликации
     */
    private void showSettingsDialog() {
        isOnSettingsScreen = true;
        Cursor cursor = dbh.getTableData(Constants.APP_SETTINGS_TABLE);
        try {
            cursor.moveToFirst();
            rowId = cursor.getString(0);
            password = cursor.getString(1);
            url = cursor.getString(2);
            manualTimeFlag = cursor.getString(3);
        } finally {
            cursor.close();
        }
        dialog.setContentView(R.layout.screen_settings);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        final EditText postUrl = (EditText) dialog.findViewById(R.id.post_url_field);
        final EditText pass_one = (EditText) dialog.findViewById(R.id.password_one);
        final EditText pass_two = (EditText) dialog.findViewById(R.id.password_two);
        Button save = (Button) dialog.findViewById(R.id.save_settings); //Кнопка сохраняющая изменения настроек
        Button reset = (Button) dialog.findViewById(R.id.reset_settings); //Кнопка сбрасывающая несохраненные изменения
        Button cancel = (Button) dialog.findViewById(R.id.close_settings); //Кнопка закрывающая экран настроек
        Button update = (Button) dialog.findViewById(R.id.update_btn); //Кнопка запускающая гугл маркет для обновления
        //Button exit = (Button) dialog.findViewById(R.id.exit_app_button); // Кнопка закрывающая всё приложение
        //CheckBox safety = (CheckBox) dialog.findViewById(R.id.exit_safety); // Checkbox снимающий предохранитель с кнопки exit
        final CheckBox manualTime = (CheckBox) dialog.findViewById(R.id.manual_input_mode); // Checkbox активирующий режим ввода времени в ручную
        LinearLayout manualTimeLayout = (LinearLayout) dialog.findViewById(R.id.manual_time_section); // Строка активации режима ручного времени
        if (Constants.APP_CLIENT.equals("WB")) {
            manualTimeLayout.setVisibility(View.VISIBLE);
        }
        /*******************************************************************************/
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newUrl = postUrl.getText().toString();
                String newPassword_1 = pass_one.getText().toString();
                String newPassword_2 = pass_two.getText().toString();
                String timeFlagCheckbox = String.valueOf(manualTime.isChecked());
                if (newPassword_1.equals(newPassword_2)) {
                    dbh.updateApplicationSettings(rowId, newUrl, newPassword_1, timeFlagCheckbox);
                    dialog.dismiss();
                    ui.setAppFullView(activity);
                } else {
                    tc.showCustomToast(11, null);
                }
            }
        });
        /*******************************************************************************/
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ui.initiateSettingsFieldsData(dialog, url, password, manualTimeFlag);
                //ui.uncheckSafetyCheckbox(dialog);
            }
        });
        /*******************************************************************************/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                isOnSettingsScreen = false;
                ui.setAppFullView(activity);
            }
        });
        /*******************************************************************************/
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = activity.getPackageName();
                try {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.GOOGLE_PLAY_URL + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.GOOGLE_PLAY_FULL_URL + appPackageName)));
                }
                dialog.dismiss();
            }
        });
        /*******************************************************************************/
//        safety.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked){
//                    ui.enableExitAppButton(dialog);
//                } else {
//                    ui.disableExitAppButton(dialog);
//                }
//            }
//        });
        /*******************************************************************************/
        manualTime.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    manualTime.setChecked(true);
                } else {
                    manualTime.setChecked(false);
                }
            }
        });
        /*******************************************************************************/
        //exit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                System.exit(0);
//            }
//        });
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        ui.initiateSettingsFieldsData(dialog, url, password, manualTimeFlag);

        // Данный кусочек кода показывает блок проверки версии в Гугл Плей в экране настроек в случае если конфигурация
        // указывает IS_ON_GPLAY == true
        if(Constants.IS_ON_GPLAY){
            ui.revealGooglePlayBlock(dialog);
            new VersionChecker().execute();
        }

        if(Constants.SHOW_BADGE_NUM){
            ui.revealBadgeNumberBlock(dialog);
        }


        /*********** Flurry Report **************************************/
        Map<String, String> flurryParams = new HashMap<String, String>();
        DataCollector.reportToFlurry(cont.getResources().getString(R.string.settings_opened), flurryParams, cont);
        /****************************************************************/

    }

    /**
     * Данный метод открывает диалог, в котором пользователь может
     * в ручную выставить время для ClockIn/ClockOut action
     *
     * @param parsedUID - серийный номер прочитанного бэджа
     */
    void showChangeTimeDialog(final String parsedUID) {
        dialog.setContentView(R.layout.screen_change_time);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        Button postData = (Button) dialog.findViewById(R.id.post_data);
        Button closeDialog = (Button) dialog.findViewById(R.id.close_time_dialog);
        final TimePicker timePicker = (TimePicker) dialog.findViewById(R.id.timePicker);

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        postData.setOnClickListener(new View.OnClickListener() {
            String hours, minutes;

            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    hours = String.valueOf(timePicker.getHour());
                    minutes = String.valueOf(timePicker.getMinute());
                } else {
                    hours = String.valueOf(timePicker.getCurrentHour());
                    minutes = String.valueOf(timePicker.getCurrentMinute());
                }
                Integer h = Integer.parseInt(hours);
                Integer m = Integer.parseInt(minutes);
                hours = h < 10 ? "0" + hours : hours;
                minutes = m < 10 ? "0" + minutes : minutes;
                MainActivity.globalAct.executeInternetConnectionCheck(parsedUID, hours + ":" + minutes + ":00");
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /**
     * Данный метод проверяет какая текущая версия приложения на данный
     * момент находится в Google Play, после чего вызывает метод проверяющий
     * нужно ли обновить приложение.
     */
    private class VersionChecker extends AsyncTask<String, String, String> {
        private String newVersion;
        final String appPackageName = activity.getPackageName();

        @Override
        protected String doInBackground(String... params) {
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + appPackageName + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return newVersion;
        }

        @Override
        protected void onPostExecute(String version) {
            super.onPostExecute(version);
            if (version != null) {
                ui.setAppVersionState(activity, dialog, version);
            }
        }

    }
}
