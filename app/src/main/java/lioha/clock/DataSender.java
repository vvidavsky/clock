package lioha.clock;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import lioha.clock.DataBase.DBHandler;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by vladvidavsky on 6/6/16.
 */
public class DataSender {
    private static ToastCustom tc;
    private static UIOperations ui;
    private static DBHandler dbh;
    private Context cont;

    DataSender(Activity activity) {
        cont = activity;
        tc = new ToastCustom(activity);
        ui = new UIOperations(activity);
        dbh = new DBHandler(activity, activity);
    }

    private static RequestBody body;

    /**
     * Метод основная цель которого создать и приобразовать данные
     * для отправки на сервак.
     *
     * @param type     стринг который указывает режим пользователя если пользователь пришел как нигретянский раб на работу или уебал с нее.
     * @param badgeNum стринг указывает серийный номер нататуированый на лбу раба
     */
    void PreparePostParams(String type, String badgeNum, String manualTime) {
        String extraData, job, date, time;
        extraData = "";
        job = "";
        time = manualTime != null ? manualTime : DataCollector.GetTimeAndDate("time");
        date = DataCollector.GetTimeAndDate("date");


        /**
         * Создаем параметры в понятном бля для сервака формате.
         * Тут конечно пришлось поебаться чутка, но, с кем не бывает.
         */
        body = new FormBody.Builder()
                .add(Constants.REPORT_TYPE, type)
                .add(Constants.REPORT_BADGE, badgeNum)
                .add(Constants.REPORT_JOB, job)
                .add(Constants.REPORT_EXTRA_DATA, extraData)
                .add(Constants.REPORT_TIME, time)
                .add(Constants.REPORT_DATE, date)
                .build();

        String baseUrl = dbh.getSettingsItemAtIndex(2);
        new SendingDataToServer().execute(baseUrl + Constants.PORT + "/swipe");

        /*********** Flurry Report **************************************/
        Map<String, String> flurryParams = new HashMap<String, String>();
        flurryParams.put("type", type);
        flurryParams.put("badge", badgeNum);
        flurryParams.put("time", time);
        flurryParams.put("date", date);
        DataCollector.reportToFlurry(cont.getResources().getString(R.string.badge_scanned), flurryParams, cont);
        /****************************************************************/
    }

    /**
     * Метод отправляющий все данные нахуй на сервак.
     */
    private class SendingDataToServer extends AsyncTask<String, String, String> {
        OkHttpClient client = new OkHttpClient();

        protected String doInBackground(String... params) {
            Request request = new Request.Builder()
                    .url(params[0])
                    .post(body)
                    .build();
            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {

                String stringResp = "no data received";
                try {
                    stringResp = String.valueOf(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                response.body().close();
                return stringResp;
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String value) {
            Map<String, String> flurryParams = new HashMap<String, String>();
            ui.hidePreloader();
            ui.setCardDefaultStatus();
            ui.setClockInInactive();
            ui.setClockOutInactive();
            MainActivity.type = null;
            if (value != null) {
                Boolean black = value.contains("black");
                Boolean green = value.contains("green");
                Boolean red = value.contains("red");
                String responseString = DataCollector.GetServerResponseMessage(value);
                if (green) {
                    tc.showCustomToast(3, responseString);
                } else if (red) {
                    tc.showCustomToast(4, responseString);
                } else if (black) {
                    tc.showCustomToast(5, responseString);
                }
                /*********** Flurry Report **************************************/
                flurryParams.put("response", responseString);
                DataCollector.reportToFlurry(cont.getResources().getString(R.string.server_resp), flurryParams, cont);
                /****************************************************************/
            } else {
                tc.showCustomToast(7, null);
                /*********** Flurry Report **************************************/
                flurryParams.put("response", cont.getResources().getString(R.string.no_server_resp));
                DataCollector.reportToFlurry(cont.getResources().getString(R.string.server_resp_fail), flurryParams, cont);
                /****************************************************************/
            }
        }

    }

}
