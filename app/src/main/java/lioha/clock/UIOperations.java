package lioha.clock;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by vladvidavsky on 10/29/16.
 */

class UIOperations {
    private LinearLayout preloader;
    private ImageView badge;
    private Button clockIn, clockOut;
    private Context context;

    UIOperations(Activity activity) {
        context = activity;
        preloader = (LinearLayout) activity.findViewById(R.id.preloader);
        badge = (ImageView) activity.findViewById(R.id.badge_status);
        clockIn = (Button) activity.findViewById(R.id.checkin);
        clockOut = (Button) activity.findViewById(R.id.checkout);
    }

    /**
     * Метод показывающий бегунок оповещающий о том
     * что пороисходит процесс отправки данных на сервер
     */
    void showPreloader() {
        preloader.setVisibility(View.VISIBLE);
    }

    /**
     * Метод прячащий бегунок
     */
    void hidePreloader() {
        preloader.setVisibility(View.INVISIBLE);
    }

    /**
     * Метод который меняет картинку ярлыка на активную
     * указывающий на то что NFC Tag успешно считался
     */
    void setCardReadStatus() {
        badge.setImageResource(R.drawable.badge_active);
    }

    /**
     * Метод возвращающий картинку ярлыка
     * NFC Tag на исходную позицию после того как
     * от данные были успешно отправлены на сервак
     */
    void setCardDefaultStatus() {
        badge.setImageResource(R.drawable.badge_neutral);
    }

    /**
     * Следующие 4 метода управляют стадиями кнопок ClockIn/ClockOut
     */
    void setClockInActive() {
        clockIn.setActivated(true);
    }

    void setClockInInactive() {
        clockIn.setActivated(false);
    }

    void setClockOutActive() {
        clockOut.setActivated(true);
    }

    void setClockOutInactive() {
        clockOut.setActivated(false);
    }
    /*****************************************************************/

    /**
     * Метод выводящий блок проверки Google Play в экране настроек
     * из спрятанного режима.
     */
    void revealGooglePlayBlock(Dialog dialog){
        LinearLayout googlePlay = (LinearLayout) dialog.findViewById(R.id.update_status_block);
        googlePlay.setVisibility(View.VISIBLE);
    }

    /**
     * Метод выводящий блок показывающий номер просканированного бэджа в экране настроек
     * из спрятанного режима.
     */
    void revealBadgeNumberBlock(Dialog dialog){
        LinearLayout badgeLine = (LinearLayout) dialog.findViewById(R.id.badge_number_line);
        badgeLine.setVisibility(View.VISIBLE);
    }

    /**
     * Метод заполняющий нужные поля экрана настроек
     *
     * @param dialog   Reference диалога в котором находятся нужнуе поля
     * @param url      Адресс сервера
     * @param password пароль для полей пароли
     */
    void initiateSettingsFieldsData(Dialog dialog, String url, String password, String manualTimeFlag) {
        TextView badgeNumber = (TextView) dialog.findViewById(R.id.scanned_badge_num);
        EditText postUrl = (EditText) dialog.findViewById(R.id.post_url_field);
        EditText pass_one = (EditText) dialog.findViewById(R.id.password_one);
        EditText pass_two = (EditText) dialog.findViewById(R.id.password_two);
        badgeNumber.setText(context.getResources().getString(R.string.badge_indicator));
        postUrl.setText(url);
        pass_one.setText(password);
        pass_two.setText(password);
        if (Constants.APP_CLIENT.equals("WB")) {
            CheckBox manualInputMode = (CheckBox) dialog.findViewById(R.id.manual_input_mode);
            manualInputMode.setChecked(Boolean.parseBoolean(manualTimeFlag));
        }
    }

    /**
     * Следующие два метода активация/деактивация кнопки выхода
     * из приложения
     * @param dialog - Reference диалога в котором находится данная кнопка
     */
//    void enableExitAppButton(Dialog dialog) {
//        Button exitApp = (Button) dialog.findViewById(R.id.exit_app_button);
//        exitApp.setEnabled(true);
//    }
//
//    void disableExitAppButton(Dialog dialog) {
//        Button exitApp = (Button) dialog.findViewById(R.id.exit_app_button);
//        exitApp.setEnabled(false);
//    }
    /******************************************************************/

    /**
     * Данный метод меняет положение Чекбокса на unchecked
     * Используется в панели настроек при нажатии на кнопку RESET
     * @param dialog - Reference диалога в котором находится данная кнопка
     */
//    void uncheckSafetyCheckbox(Dialog dialog) {
//        CheckBox exitAppSafety = (CheckBox) dialog.findViewById(R.id.exit_safety);
//        exitAppSafety.setChecked(false);
//    }

    /**
     * Метод прячащий контрольные кнопки дивайса
     * позволяющий сделать приложение полнометражным
     *
     * @param activity - в которой находится главный лейаут
     */
    void setAppFullView(Activity activity) {
        activity.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_FULLSCREEN |
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    /**
     * Данный метод отображает номер текущей версии приложения
     *
     * @param activity - main activity instance
     */
    void setVersionNumber(Activity activity) {
        String version = DataCollector.getCurrentAppVersion(activity);
        final TextView versionText = (TextView) activity.findViewById(R.id.version_num);
        assert version != null;
        String text = "V.0." + version;
        versionText.setText(text);
    }

    /**
     * Данный метод получает данные о версии приложения в Google Play
     * и если существует более новая версия, оповещает пользователя
     * либо указывает что текущая версия приложения не нуждается в обновлении
     * @param activity         - текущая активность от куда был отправлен метод
     * @param dialog           - экран в котором нужно произвести изменения
     * @param playStoreVersion - версия приложения в Google Play
     */
    void setAppVersionState(Activity activity, Dialog dialog, String playStoreVersion) {
        final TextView statusText = (TextView) dialog.findViewById(R.id.version_status_line);
        ProgressBar statusPreloader = (ProgressBar) dialog.findViewById(R.id.version_preloader);
        Button updateButton = (Button) dialog.findViewById(R.id.update_btn);
        final String currentVersion = "0." + DataCollector.getCurrentAppVersion(activity);
        String textForStatus = activity.getResources().getString(R.string.no_update_available_msg) + " " + currentVersion;

        if (playStoreVersion != null && currentVersion != null && !currentVersion.equals("0." + playStoreVersion)) {
            textForStatus = activity.getResources().getString(R.string.cur_version) + currentVersion +
            activity.getResources().getString(R.string.up_to) + " " + "0." + playStoreVersion + " " + activity.getResources().getString(R.string.is_available);
            statusText.setTextColor(ResourcesCompat.getColor(activity.getResources(), R.color.system_green, null));
            updateButton.setVisibility(View.VISIBLE);
        }
        statusPreloader.setVisibility(View.GONE);
        statusText.setText(textForStatus);
    }

    /**
     * Метод пропечатывающий номер просканированного бэджа
     * в панеле настроек.
     * @param dialog - instance панели настроек
     * @param badgeNumber - полученный номер бэджа
     */
    void showBadgeNumberInSettingsScreen(Dialog dialog, String badgeNumber){
        TextView badgeLine = (TextView) dialog.findViewById(R.id.scanned_badge_num);
        badgeLine.setText(badgeNumber);
    }
}
