package lioha.clock;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import com.flurry.android.FlurryAgent;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import lioha.clock.DataBase.DBHandler;
import lioha.clock.ForeignAppsManager.ForeignAppsManager;


public class MainActivity extends Activity implements View.OnClickListener {

    public NfcAdapter mNfcAdapter;
    public static String type;

    protected NfcAdapter nfcAdapter;
    protected PendingIntent nfcPendingIntent;

    public DataSender dataSender;
    private ToastCustom tc;
    private DialogCustom dc;
    private UIOperations ui;

    public static DBHandler dbh;
    public static MainActivity globalAct;
    public ForeignAppsManager fam;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(onRuntimeError);
        setContentView(R.layout.activity_main);

        /**
         * Здесь мы инициализируем базу данных и создаем
         * первичную таблицу с данными о настройках, по мере надобности.
         */
        dbh = new DBHandler(MainActivity.this, MainActivity.this);
        if (dbh.checkIfSettingsAreEmpty()) {
            dbh.createSettingsTable();
        }

        /**
         * Данный кусочек кода держит экран постоянно включеным пока данное
         * приложение не выйдет из Main Thread
         */
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        /********************************************************************************/

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        tc = new ToastCustom(MainActivity.this);
        dc = new DialogCustom(MainActivity.this);
        ui = new UIOperations(MainActivity.this);
        dataSender = new DataSender(MainActivity.this);
        fam = new ForeignAppsManager(MainActivity.this);
        globalAct = MainActivity.this;

        ImageButton settingsScreen = (ImageButton) findViewById(R.id.settings_screen);
        Button clockIn = (Button) findViewById(R.id.checkin);
        Button clockOut = (Button) findViewById(R.id.checkout);
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        clockIn.setOnClickListener(this);
        clockOut.setOnClickListener(this);
        settingsScreen.setOnClickListener(this);
        ui.setVersionNumber(MainActivity.this);
        ui.setAppFullView(MainActivity.this);

        // initialize NFC
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        new FlurryAgent.Builder().withLogEnabled(true).build(this, Constants.FLURRY_API_KEY);

//        new Timer().scheduleAtFixedRate(new TimerTask(){
//            @Override
//            public void run(){
//                mHandler.obtainMessage(1).sendToTarget();
//            }
//        },0,100);
    }

//    public Handler mHandler = new Handler() {
//        public void handleMessage(Message msg) {
//            ui.setAppFullView(MainActivity.this);
//        }
//    };


    public void enableForegroundMode() {
        Log.d(Constants.TAG, "enableForegroundMode");

        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED); // filter for all
        IntentFilter[] writeTagFilters = new IntentFilter[]{tagDetected};
        nfcAdapter.enableForegroundDispatch(this, nfcPendingIntent, writeTagFilters, null);
    }

    public void disableForegroundMode() {
        Log.d(Constants.TAG, "disableForegroundMode");
        nfcAdapter.disableForegroundDispatch(this);
    }

    /**
     * Данный метод перезапускает приложение в случае если оно
     * падает
     */
    private Thread.UncaughtExceptionHandler onRuntimeError = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread thread, Throwable ex) {
            sendBroadcast(new Intent(Constants.RESURRECT_STRING));
        }
    };

    /**
     * Оверайдим кнопку Back на самом гаджете
     * чтобы она не реагировала на нажатия пользователя
     */
    @Override
    public void onBackPressed() {
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.checkin:
                if (!v.isActivated()) {
                    ui.setClockInActive();
                    ui.setClockOutInactive();
                    type = "01";
                } else {
                    ui.setClockInInactive();
                    type = null;
                }
                break;
            case R.id.checkout:
                if (!v.isActivated()) {
                    ui.setClockInInactive();
                    ui.setClockOutActive();
                    type = "02";
                } else {
                    ui.setClockOutInactive();
                    type = null;
                }
                break;
            case R.id.settings_screen:
                dc.showSettingsPasswordDialog();
                break;
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        Log.d(Constants.TAG, "onNewIntent");
        String parsedUID = DataCollector.ByteArrayToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID));
        if (type != null || DialogCustom.isOnSettingsScreen) {
            if (parsedUID != null) {
                if (DialogCustom.isOnSettingsScreen) {
                    ui.showBadgeNumberInSettingsScreen(dc.dialog, parsedUID);
                } else {
                    if (dbh.isManualTimePickerEnabled()) {
                        dc.showChangeTimeDialog(parsedUID);
                    } else {
                        executeInternetConnectionCheck(parsedUID, null);
                    }
                }
            } else {
                tc.showCustomToast(2, null);
            }
        } else {
            tc.showCustomToast(1, null);
        }
    }

    @Override
    protected void onResume() {
        Log.d(Constants.TAG, "onResume");
        super.onResume();
        if (nfcAdapter != null) {
            enableForegroundMode();
        }
    }

    @Override
    protected void onPause() {
        Log.d(Constants.TAG, "onPause");
        super.onPause();
        if (nfcAdapter != null) {
            disableForegroundMode();
        }
    }

    /**
     * Данный метод исполняет запуск не синхронного запроса
     * проверяющего есть ли выход в интернет
     *
     * @param parsedUID - серийный номер прочитанного бэджа
     * @param time      - время внесенное вручную (опциональный параметр)
     */
    public void executeInternetConnectionCheck(String parsedUID, String time) {
        new InternetConnectionCheck().execute(parsedUID, time);
    }

    /**
     * Метод проверяющий есть ли выход в интернет перед отправкой данных
     */
    private class InternetConnectionCheck extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ui.showPreloader();
            ui.setCardReadStatus();
        }

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        private String badgeId, time;

        @Override
        protected String doInBackground(String... params) {
            badgeId = params[0];
            time = params[1]; // TODO Данный параметр приходит только в случае когда пользователь в ручную вводит время - WB Demo feature, стерететь нахуй если не понадобится
            String connectionIsOk = null;
            if (activeNetworkInfo != null) {
                try {
                    HttpURLConnection urlc = (HttpURLConnection)
                            (new URL("http://clients3.google.com/generate_204")
                                    .openConnection());
                    urlc.setRequestProperty("User-Agent", "Android");
                    urlc.setRequestProperty("Connection", "close");
                    urlc.setConnectTimeout(1500);
                    urlc.connect();
                    connectionIsOk = "CONNECTED";
                    return connectionIsOk;
                } catch (IOException e) {

                }
            }
            return connectionIsOk;
        }

        @Override
        protected void onPostExecute(String connectionIsOk) {
            if (connectionIsOk != null && connectionIsOk.equals("CONNECTED")) {
                dataSender.PreparePostParams(type, badgeId, time);
            } else {
                tc.showCustomToast(6, null);
                ui.hidePreloader();
                ui.setCardDefaultStatus();
                String reportTime = DataCollector.GetTimeAndDate("time");
                String reportDate = DataCollector.GetTimeAndDate("date");
                dbh.backupUnsentReport(type, badgeId, null, null, reportTime, reportDate);
                dbh.getBackupData();
            }
        }
    }



}
