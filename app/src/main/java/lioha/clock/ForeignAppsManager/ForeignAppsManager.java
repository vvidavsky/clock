package lioha.clock.ForeignAppsManager;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import lioha.clock.Constants;
import lioha.clock.MainActivity;

/**
 * Created by vlad on 20/05/2017.
 */

public class ForeignAppsManager extends Thread {
    private Context cont;

    public ForeignAppsManager(Activity act) {
        cont = act;

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        if (Constants.IS_ON_GPLAY) {
                            stopApplication(Constants.GOOGLE_PLAY_PACKAGE_NAME);
                            launchApplication(Constants.GOOGLE_PLAY_PACKAGE_NAME);
                            focusOnClockApp();
                        } else {
                            Boolean appIsRunning = isAppRunning(Constants.AUTO_UPDATER_PACKAGE_NAME);
                            if (!appIsRunning) {
                                launchApplication(Constants.AUTO_UPDATER_PACKAGE_NAME);
                                focusOnClockApp();
                            }
                        }
                    }
                }, Constants.UPDATER_CHECK_INTERVAL);


    }

    /**
     * Метод возвращающий Clock app в Main Thread после того как другое приложение было
     * перезапущено выбив Clock app из Main Thread.
     */
    private void focusOnClockApp() {
        Intent it = new Intent("android.intent.action.MAIN");
        it.setComponent(new ComponentName(cont.getPackageName(), MainActivity.class.getName()));
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        cont.getApplicationContext().startActivity(it);
    }

    /**
     * Метод отключающий бегущий процесс/аппликацию.
     *
     * @param processName - имя процесса который нужно вырубить к хуям.
     */
    private void stopApplication(String processName) {
        ActivityManager am = (ActivityManager) cont.getSystemService(Activity.ACTIVITY_SERVICE);
        am.killBackgroundProcesses(processName);
        Log.d(Constants.TAG, "stopApplication: application stopped");
    }

    /**
     * Метод запускающий процесс/аппликацию находящийся на аппарате.
     *
     * @param processName - имя процесса который нужно запустить.
     */
    private void launchApplication(String processName) {
        Intent intent = cont.getPackageManager().getLaunchIntentForPackage(processName);
        if (intent != null) {
            cont.startActivity(intent);
            Log.d(Constants.TAG, "launchApplication: target app is launched");
        } else {
            Log.d(Constants.TAG, "launchApplication: target app package not found");
        }
    }

    /**
     * Метод проверяющий статус запрошенного приложения, бежит ли оно либо выключено.
     *
     * @param packageName - имя приложения чей статус нужно проверить.
     * @return Boolean
     */
    private boolean isAppRunning(final String packageName) {
        final ActivityManager activityManager = (ActivityManager) cont.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null) {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.processName.equals(packageName)) {
                    return true;
                }
            }
        }
        return false;
    }
}
