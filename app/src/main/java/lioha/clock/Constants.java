package lioha.clock;

/**
 * Created by vladvidavsky on 6/7/16.
 */
public class Constants {
    public static final String BASE_URL = "http://54.245.121.216"; //Начальный URL отправки данных на сервер для экрана настроек
    public static final String PORT = ":8500"; //Начальный URL отправки данных на сервер для экрана настроек
    public static final String GOOGLE_PLAY_URL = "market://details?id="; //URL с префиксом Гугл маркета ищущий приложение по ID
    public static final String GOOGLE_PLAY_FULL_URL = "https://play.google.com/store/apps/details?id="; // Полный URL приложения в Гугл маркете
    public static final String RESURRECT_STRING = "RESURRECT"; //Название события вызывающий перезагрузку приложения в случае крушения
    public static final String SETTINGS_PWD = "1234"; //Начальный пароль для экрана настроек
    public static final String ADMIN_PWD = "170883"; //Статичный и неизменимый пароль одинаковый для всех дивайсов
    public static final String FLURRY_API_KEY = "JRSB2CPJW7B6W2M3HB9C"; //API KEY модуля аналитики пользования приложением
    public static final String GOOGLE_PLAY_PACKAGE_NAME = "com.android.vending"; //Название пакета Гугл Маркета
    public static final String AUTO_UPDATER_PACKAGE_NAME = "com.sannacode.android.myapplication"; //Название пакета утилиты обновляющей данное приложение
    public static final String TAG = MainActivity.class.getName();

    /************** Константы с названиями полей таблицы настроек панели управление *******************/
    public static final String APP_SETTINGS_TABLE = "clock_settings"; //Название таблицы с настройками приложения в базе данных.
    public static final String PWD_FIELD = "settings_password"; //Поле содержащее пароль для входа в экран настроек
    public static final String POST_URL_FIELD = "post_url"; //Поле содержащее URL для отправки данных аппликации
    public static final String MANUAL_TIME_SETTINGS = "manual_time"; //Поле указывающее на то активен ли режим ввода времени в ручную или нет
    /**************************************************************************************************/

    /************** Константы с названиями полей таблицы не сохраненных рапортов **********************/
    public static final String APP_BACKUP_TABLE = "reports_backup";
    public static final String REPORT_TYPE = "type";
    public static final String REPORT_BADGE = "badge";
    public static final String REPORT_JOB = "job";
    public static final String REPORT_EXTRA_DATA = "extraData";
    public static final String REPORT_TIME = "time";
    public static final String REPORT_DATE = "date";
    /**************************************************************************************************/

    public static final Integer UPDATER_CHECK_INTERVAL = 1000 * 60 * 15; // Временной интервал для проверки и перезапуска внешних приложений

    /************** Константы конфигураций режимов работы аппликации **********************************/
    public static final String APP_CLIENT = ""; // WB Константа активирующая разные возможности приложения индивидуально настреные под определенного клиента
    public static final Boolean IS_ON_GPLAY = true; // Константа указывающая на то нужно ли осуществлять проверку новой версии в гугл плэй.
    public static final Boolean SHOW_BADGE_NUM = true; // Константа указывающая на то нужно ли показывать строку с номером бэджа в панеле настроек
    /**************************************************************************************************/
}