package lioha.clock.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import lioha.clock.Constants;

/**
 * Created by vladvidavsky on 10/24/16.
 */

class DBHelper extends SQLiteOpenHelper {

    DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String settingsTable = ("CREATE TABLE " + Constants.APP_SETTINGS_TABLE + " (_id INTEGER PRIMARY KEY, " + Constants.PWD_FIELD + " TEXT, " + Constants.POST_URL_FIELD + " TEXT, " + Constants.MANUAL_TIME_SETTINGS + " TEXT)");
        String reportsBackupTable = ("CREATE TABLE " + Constants.APP_BACKUP_TABLE + "(_id INTEGER PRIMARY KEY, " + Constants.REPORT_TYPE + " TEXT, " + Constants.REPORT_BADGE + " TEXT, " + Constants.REPORT_JOB + " TEXT, " + Constants.REPORT_EXTRA_DATA + " TEXT, " + Constants.REPORT_TIME + " TEXT, " + Constants.REPORT_DATE + " TEXT)");
        try {
            db.execSQL(settingsTable);
            db.execSQL(reportsBackupTable);
        } catch (SQLiteException e) {
            e.getCause();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
