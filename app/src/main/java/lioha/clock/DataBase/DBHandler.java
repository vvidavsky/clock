package lioha.clock.DataBase;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import lioha.clock.Constants;
import lioha.clock.ToastCustom;

/**
 * Created by vladvidavsky on 10/24/16.
 */

public class DBHandler {
    private DBHelper dbHelper;
    private ToastCustom tc;

    public DBHandler(Context context, Activity activity) {
        dbHelper = new DBHelper(context, "ClockIO.db", null, 1);
        tc = new ToastCustom(activity);
    }

    /**
     * Данный метод проверяет существуют ли данные в таблице настроек
     * либо она пуста.
     *
     * @return Boolean
     */
    public Boolean checkIfSettingsAreEmpty() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String count = "SELECT count(*) FROM " + Constants.APP_SETTINGS_TABLE;
        Cursor cursor = db.rawQuery(count, null);
        cursor.moveToFirst();
        int length = cursor.getInt(0);
        cursor.close();
        return length == 0;
    }

    /**
     * Метод создающий объект с данными о настройках приложения
     * исполъзуется только один раз при самом первом запуске приложения
     * для того чтобы создать первоначальные настройки в базе данных
     */
    public void createSettingsTable() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(Constants.PWD_FIELD, Constants.SETTINGS_PWD);
            values.put(Constants.POST_URL_FIELD, Constants.BASE_URL);
            values.put(Constants.MANUAL_TIME_SETTINGS, "false");
            database.insertOrThrow(Constants.APP_SETTINGS_TABLE, null, values);
        } catch (SQLiteException e) {
            e.getCause();
        } finally {
            if (database.isOpen()) database.close();
        }
    }

    /**
     * Данный метод осуществляет сохранение изменений в панели настроек
     * @param rowId    - ID строки которую нужно обновить.
     * @param url      - адресс сервера для отправки данных.
     * @param password - пароль панели настроек
     */
    public void updateApplicationSettings(String rowId, String url, String password, String manual_time) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(Constants.POST_URL_FIELD, url);
            values.put(Constants.PWD_FIELD, password);
            values.put(Constants.MANUAL_TIME_SETTINGS, manual_time);
            database.update(Constants.APP_SETTINGS_TABLE, values, "_id = ?", new String[]{rowId});
            tc.showCustomToast(10, null);
        } catch (SQLiteException e) {
            e.getCause();
        } finally {
            if (database.isOpen()) database.close();
        }
    }

    /**
     * Данный метод сохраняет данные сосканированного NFC/RFID в базе данных.
     */
    public void backupUnsentReport(String type, String badge, String job, String extraData, String time, String date) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(Constants.REPORT_TYPE, type);
            values.put(Constants.REPORT_BADGE, badge);
            values.put(Constants.REPORT_JOB, job);
            values.put(Constants.REPORT_EXTRA_DATA, extraData);
            values.put(Constants.REPORT_TIME, time);
            values.put(Constants.REPORT_DATE, date);
            database.insertOrThrow(Constants.APP_BACKUP_TABLE, null, values);
        } catch (SQLiteException e) {
            e.getCause();
        } finally {
            if (database.isOpen()) database.close();
        }
    }

    /**
     * Метод возвращающий данные указаной даблицы
     * @param tableName - String название таблицы в базе данных из которой нужно вытащить результаты
     */
    public Cursor getTableData(String tableName) {
        Cursor cursor = null;
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        try {
            cursor = database.query(tableName, null, null, null, null, null, null);
        } catch (SQLiteException e) {
            e.getCause();
        }
        return cursor;
    }

    /**
     * Данная функция стерает все записи из определенной таблицы в базе данных
     * @param tableName имя таблицы которую нужно очистить.
     */
    public void cleanDbTable(String tableName){
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        String deleteQuery = ("DELETE FROM " + tableName);
        try {
            database.execSQL(deleteQuery);
        } catch (SQLiteException e) {
            e.getCause();
        }
    }

    /**
     * Метод возвращающий статус активности панели ручного ввода времени из
     * базы данных.
     *
     * @return Boolean true/false
     */
    public Boolean isManualTimePickerEnabled() {
        Boolean manualTimeFlag;
        Cursor cursor = getTableData(Constants.APP_SETTINGS_TABLE);
        try {
            cursor.moveToFirst();
            manualTimeFlag = Boolean.valueOf(cursor.getString(3));
        } finally {
            cursor.close();
        }
        return manualTimeFlag;
    }

    /**
     * Метод возвращающий значение определенной ячейки из базы данных
     *
     * @param index Integer желаемый индех ячейки в базе данных
     * @return String значение полученное из базы данных
     */
    public String getSettingsItemAtIndex(Integer index) {
        String valueToReturn;
        Cursor cursor = getTableData(Constants.APP_SETTINGS_TABLE);
        try {
            cursor.moveToFirst();
            valueToReturn = cursor.getString(index);
        } finally {
            cursor.close();
        }
        return valueToReturn;
    }

    public void getBackupData() {
        Cursor cursor = getTableData(Constants.APP_BACKUP_TABLE);
        while (cursor.moveToNext()) {
            String type = cursor.getString(1),
                    badge = cursor.getString(2),
                    sqlDetails = cursor.getString(3),
                    sqlCertificate = cursor.getString(4),
                    sqlPrice = cursor.getString(5),
                    sqlCamera = cursor.getString(6);
            System.out.println(type + " " + badge + " " + sqlDetails + " " + sqlCertificate + " " + sqlPrice + " " + sqlCamera);
        }
    }
}
